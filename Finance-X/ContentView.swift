//
//  ContentView.swift
//  Finance-X
//
//  Created by Leon Bonnes on 20/11/2020.
//

import SwiftUI

let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0)

struct ContentView : View {
@State var username: String = ""
@State var password: String = ""
@State var action: Bool = false
@State var isActive = false
    
    var body: some View {
        
        NavigationView {
            
            VStack {
                Text("Welcome!")
                    .font(.largeTitle)
                    .fontWeight(.semibold)
                    .padding(.bottom, 20)
                Image("FINANCEX")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 150, height: 150)
                    .clipped()
                    .padding(.bottom, 75)
                TextField("Username", text: $username)
                    .padding()
                    .background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding(.bottom, 20)
                SecureField("Password", text: $password)
                    .padding()
                    .background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding(.bottom, 20)
                NavigationLink(destination: LandingScreenView(), isActive: $action) {
                    EmptyView()
                }
                
                Text("LOGIN!")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .frame(width: 220, height: 60)
                    .background(Color.blue)
                    .cornerRadius(15.0)
                    .onTapGesture {
                        self.clicked()
                }
            }
            .padding()
                }
            }
            
            func clicked() {
                action = true
    }
}
