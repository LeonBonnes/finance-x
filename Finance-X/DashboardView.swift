//
//  DashboardView.swift
//  Finance-X
//
//  Created by Leon Bonnes on 20/11/2020.
//

import SwiftUI

struct DashboardView : View {
    
@State var action: Bool = false
    
    var body: some View {
        
        NavigationView {
            
        VStack {
            Text("Dashboard")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .padding(.bottom, 20)
            Text("Current Account")
                .fontWeight(.semibold)
            Text("Account Number")
                .fontWeight(.semibold)
            Text("12437392")
                .fontWeight(.semibold)
            Text("Sort Code")
                .fontWeight(.semibold)
            Text("98-02-13")
                .fontWeight(.semibold)
            Text("Balance: £1220.00")
                .fontWeight(.semibold)
                .padding(.bottom, 30)
            
            NavigationLink(destination: RecentTransactionsView(), isActive: $action) {
                EmptyView()
            }
            
            Text("Recent Transactions")
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .frame(width: 220, height: 60)
                .background(Color.blue)
                .cornerRadius(15.0)
                .onTapGesture {
                    self.clicked()
            }
            
        }
        .padding()
            }
    }
    func clicked() {
        action = true
    }
}
