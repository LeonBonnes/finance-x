//
//  Finance_XApp.swift
//  Finance-X
//
//  Created by Leon Bonnes on 20/11/2020.
//

import SwiftUI

@main
struct Finance_XApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
