//
//  LandingScreen.swift
//  Finance-X
//
//  Created by Leon Bonnes on 20/11/2020.
//

import SwiftUI

struct LandingScreenView : View {
@State var action: Bool = false
@State var isActive = false
    
    var body: some View {
        NavigationView {
            VStack(spacing:30) {
            Text("Menu")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .padding(.bottom, 20)

            NavigationLink(destination: DashboardView(), isActive: $action) {
                EmptyView()
            }
            
            Text("Dashboard")
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .frame(width: 220, height: 60)
                .background(Color.blue)
                .cornerRadius(15.0)
                .onTapGesture {
                    self.clicked()
            }
                
            NavigationLink(destination: SavingAccountsView(), isActive: $action) {
                EmptyView()
                }
                
            Text("Saving Accounts")
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .frame(width: 220, height: 60)
                .background(Color.blue)
                .cornerRadius(15.0)
                .onTapGesture {
                    self.clicked()
                }
                
            NavigationLink(destination: SavingPlanView(), isActive: $action) {
                EmptyView()
                }
                
            Text("Saving Plan")
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .frame(width: 220, height: 60)
                .background(Color.blue)
                .cornerRadius(15.0)
                .onTapGesture {
                    self.clicked()
                }
        }
        .padding()
            }
        }
        
        func clicked() {
            action = true
}
}
