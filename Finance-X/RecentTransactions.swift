//
//  RecentTransactions.swift
//  Finance-X
//
//  Created by Leon Bonnes on 20/11/2020.
//

import SwiftUI

struct RecentTransactionsView : View {
@State var action: Bool = false
    
    var body: some View {
        NavigationView {
        VStack {
            Text("Recent Transactions")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .padding(.bottom, 20)
        }
        .padding()
            }
        }
}
