//
//  SavingAccounts.swift
//  Finance-X
//
//  Created by Leon Bonnes on 20/11/2020.
//

import SwiftUI

struct SavingAccountsView : View {
@State var action: Bool = false
    
    var body: some View {
        NavigationView {
        VStack {
            Text("Saving Accounts")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .padding(.bottom, 20)
            Image("FINANCEX")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 150, height: 150)
                .clipped()
                .padding(.bottom, 75)
        
        }
        .padding()
            }
        }
}
